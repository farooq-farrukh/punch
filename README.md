# README

Some Important Points To Consider:

* Ruby version: 2.3.0

* Rails version: 5.0

* Created with the API-only app facility of Rails 5

* Inheritance and metaprogramming is used at instances where this would compact the code

* `bundle exec rspec` is the command to run the specs

* Each API endpoint has a respective spec against it

* Test coverage is limited due to the limited amount of time for this assignment which can be further improved given enough time

* Taking cues from real world scenarios, some assumptions are made:
  - Projects must belong to a client at all times, they can not exist without a client.
  - Employee can be assigned to and removed from a project so employee can exist as a separate entity.