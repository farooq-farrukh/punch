class ApplicationController < ActionController::API
  include Response
  include ExceptionHandler

  before_action :get_record, except: :create

  def show
    json_response(@record)
  end

  def update
    @record.update!(name: params[:name])
    json_response(@record)
  end

  private
  def get_record
    @record = class_eval(params[:controller].classify).find(params[:id])
  end
end
