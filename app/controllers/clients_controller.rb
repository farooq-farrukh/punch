class ClientsController < ApplicationController
  def create
    @client = Client.create!(name: params[:name])
    json_response(@client, :created)
  end
end
