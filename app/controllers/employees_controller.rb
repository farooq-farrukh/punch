class EmployeesController < ApplicationController
  def create
    @employee = Employee.create!(name: params[:name])
    json_response(@employee, :created)
  end
end
