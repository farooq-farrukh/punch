class ProjectsController < ApplicationController
  before_action :get_client, only: :create
  before_action :get_employee, only: [:assign_employee, :remove_employee]

  def create
    @project = @client.projects.create!(name: params[:name])
    json_response(@project, :created)
  end

  def destroy
    @record.destroy
    json_response(nil, :no_content)
  end

  def assign_employee
    @record.employees << @employee
    json_response(@employee)
  end

  def remove_employee
    @record.employees.delete(@employee)
    json_response(@employee)
  end

  private
  def get_client
    @client = Client.find(params[:client_id])
  end

  def get_employee
    @employee = Employee.find(params[:employee_id])
  end
end
