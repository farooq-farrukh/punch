class Employee < ApplicationRecord
  validates :name, presence: true
  belongs_to :project, optional: true
end
