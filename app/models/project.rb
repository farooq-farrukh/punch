class Project < ApplicationRecord
  validates :name, presence: true
  belongs_to :client
  has_many :employees
end
