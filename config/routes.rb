Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :employees

  resources :clients do
    resources :projects do
      member do
        post 'assign_employee', 'remove_employee'
      end
    end
  end
end
