require 'rails_helper'

describe "POST /clients", type: :request do
  context 'when valid params' do
    it "returns status 201" do
      post "/clients", params: {name: "test_client"}
      expect(response).to have_http_status(201)
    end
  end

  context 'when invalid params' do
    it "returns status 422" do
      post "/clients", params: {name: nil}
      expect(response).to have_http_status(422)
    end
  end
end

describe "GET /clients/:id", type: :request do
  context 'when record present' do
    create_client

    it "returns status 200" do
      get "/clients/#{client.id}"
      expect(response).to have_http_status(200)
    end
  end

  context 'when record not present' do
    it "returns status 404" do
      get "/clients/0"
      expect(response).to have_http_status(404)
    end
  end
end

describe "PUT /clients/:id", type: :request do
  create_client

  context 'when valid params' do
    it "returns status 200" do
      put "/clients/#{client.id}", params: {name: "new_name"}
      expect(response).to have_http_status(200)
    end
  end

  context 'when invalid params' do
    it "returns status 422" do
      put "/clients/#{client.id}", params: {name: nil}
      expect(response).to have_http_status(422)
    end
  end
end