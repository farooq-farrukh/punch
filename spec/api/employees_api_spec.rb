require 'rails_helper'

describe "POST /employees", type: :request do
  context 'when valid params' do
    it "returns status 201" do
      post "/employees", params: {name: "test_employee"}
      expect(response).to have_http_status(201)
    end
  end

  context 'when invalid params' do
    it "returns status 422" do
      post "/employees", params: {name: nil}
      expect(response).to have_http_status(422)
    end
  end
end

describe "GET /employees/:id", type: :request do
  context 'when record present' do
    create_employee

    it "returns status 200" do
      get "/employees/#{employee.id}"
      expect(response).to have_http_status(200)
    end
  end

  context 'when record not present' do
    it "returns status 404" do
      get "/employees/0"
      expect(response).to have_http_status(404)
    end
  end
end

describe "PUT /employees/:id", type: :request do
  create_employee

  context 'when valid params' do
    it "returns status 200" do
      put "/employees/#{employee.id}", params: {name: "new_name"}
      expect(response).to have_http_status(200)
    end
  end

  context 'when invalid params' do
    it "returns status 422" do
      put "/employees/#{employee.id}", params: {name: nil}
      expect(response).to have_http_status(422)
    end
  end
end