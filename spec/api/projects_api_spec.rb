require 'rails_helper'

describe "POST /clients/:client_id/projects", type: :request do
  create_client

  context 'when valid params' do
    it "returns status 201" do
      post "/clients/#{client.id}/projects", params: {name: "test_project"}
      expect(response).to have_http_status(201)
    end
  end

  context 'when invalid params' do
    it "returns status 422" do
      post "/clients/#{client.id}/projects", params: {name: nil}
      expect(response).to have_http_status(422)
    end
  end
end

describe "GET /clients/:client_id/projects/:id", type: :request do
  create_client

  context 'when record present' do
    create_project

    it "returns status 200" do
      get "/clients/#{client.id}/projects/#{project.id}"
      expect(response).to have_http_status(200)
    end
  end

  context 'when record not present' do
    it "returns status 404" do
      get "/clients/#{client.id}/projects/0"
      expect(response).to have_http_status(404)
    end
  end
end

describe "PUT /clients/:client_id/projects/:id", type: :request do
  create_client
  create_project

  context 'when valid params' do
    it "returns status 200" do
      put "/clients/#{client.id}/projects/#{project.id}", params: {name: "new_name"}
      expect(response).to have_http_status(200)
    end
  end

  context 'when invalid params' do
    it "returns status 422" do
      put "/clients/#{client.id}/projects/#{project.id}", params: {name: nil}
      expect(response).to have_http_status(422)
    end
  end
end

describe "DELETE /clients/:client_id/projects/:id", type: :request do
  create_client
  create_project

  context 'when record present' do
    it "returns status 204" do
      delete "/clients/#{client.id}/projects/#{project.id}"
      expect(response).to have_http_status(204)
    end
  end

  context 'when record not present' do
    it "returns status 404" do
      put "/clients/#{client.id}/projects/0"
      expect(response).to have_http_status(404)
    end
  end
end

describe "POST /clients/:client_id/projects/:id/assign_employee", type: :request do
  create_records
  let(:path) { "/clients/#{client.id}/projects/#{project.id}/assign_employee" }

  context 'when valid params' do
    it "employee is assigned" do
      post path, params: {employee_id: employee.id}
      expect(response).to have_http_status(200)
    end
  end

  context 'when invalid params' do
    it "returns status 404" do
      post path, params: {employee_id: 0}
      expect(response).to have_http_status(404)
    end
  end

  context 'when records present' do
    it "employee is assigned" do
      post path, params: {employee_id: employee.id}
      expect(response).to have_http_status(200)
    end
  end

  context 'when records not present' do
    it "returns status 404" do
      post "/clients/#{client.id}/projects/0/assign_employee", params: {employee_id: employee.id}
      expect(response).to have_http_status(404)
    end
  end
end

describe "POST /clients/:client_id/projects/:id/remove_employee", type: :request do
  create_records
  let(:path) { "/clients/#{client.id}/projects/#{project.id}/remove_employee" }

  before :each do
    assign_employee
  end

  context 'when valid params' do
    it "employee is removed" do
      post path, params: {employee_id: employee.id}
      expect(response).to have_http_status(200)
    end
  end

  context 'when invalid params' do
    it "returns status 404" do
      post path, params: {employee_id: 0}
      expect(response).to have_http_status(404)
    end
  end

  context 'when records present' do
    it "employee is removed" do
      post path, params: {employee_id: employee.id}
      expect(response).to have_http_status(200)
    end
  end

  context 'when records not present' do
    it "returns status 404" do
      post "/clients/#{client.id}/projects/0/assign_employee", params: {employee_id: employee.id}
      expect(response).to have_http_status(404)
    end
  end
end